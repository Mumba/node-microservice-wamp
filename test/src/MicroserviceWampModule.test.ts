/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const Sandal = require('sandal');
import * as assert from 'assert';
import { Config } from 'mumba-config';
import { DiContainer } from 'mumba-typedef-dicontainer';
import { MicroserviceWampModule } from '../../src/index';
import { WampClient, WampSession } from 'mumba-wamp';

describe('MicroserviceLoggerModule integration tests', () => {
	let container: DiContainer;

	beforeEach(() => {
		container = new Sandal();
		container.object(
			'config',
			new Config({
				wamp: {
					url: 'ws://mumba__crossbar:8080',
					realm: 'test-realm',
					authid: '1',
					authmethods: ['wampcra'],
					secret: '*password'
				}
			})
		);
	});

	it('should register artifacts in the container', done => {
		MicroserviceWampModule.register(container);

		assert.strictEqual(container.has('exit'), true, 'should have exit');
		assert.strictEqual(
			container.has('wampClient'),
			true,
			'should have wampClient'
		);
		assert.strictEqual(
			container.has('wampSessionApi'),
			true,
			'should have wampSessionApi'
		);

		// TODO Add a TypeDef for a logger.
		container.resolve(
			(err: Error, wampClient: WampClient, wampSessionApi: WampSession) => {
				if (err) {
					return done(err);
				}

				assert(
					wampClient instanceof WampClient,
					'should be a WampClient object'
				);
				assert(
					wampSessionApi instanceof WampSession,
					'should be a WampClient object'
				);
				done();
			}
		);
	});

	it('should log a WAMP connection', done => {
		const wampClient = new WampClient();
		const logger = {
			debug: (details: any, msg: string) => {
				assert(details.session_id, 'should be the details');
				done();
			}
		};

		wampClient.openConnection = () => void 0;

		MicroserviceWampModule.init(wampClient, {}, logger, () => process.exit());
		wampClient.onOpen.next({ id: 123 });
	});

	it('should log a WAMP disconnection', done => {
		const wampClient = new WampClient();
		const logger = {
			debug: (details: any, msg: string) => {
				assert(details.details.foo, 'should be the details');
				assert(details.url, 'should be the connection url');
				done();
			}
		};

		wampClient.openConnection = () => void 0;

		MicroserviceWampModule.init(wampClient, { url: 'url' }, logger, () =>
			process.exit()
		);
		wampClient.onClose.next({ foo: 'bar' });
	});

	it('should log a WAMP error', done => {
		const wampClient = new WampClient();
		const logger = {
			error: (details: any, msg: string) => {
				assert(details.err instanceof Error, 'should be the details');
				assert.equal(msg, 'the-error', 'should be the connection msg');
				done();
			}
		};

		wampClient.openConnection = () => void 0;

		MicroserviceWampModule.init(wampClient, {}, logger, process.exit);
		wampClient.onError.next(new Error('the-error'));
	});

	it('should exit if a proc is already registered', done => {
		const wampClient = new WampClient();
		const error = { error: 'wamp.error.procedure_already_exists' };
		const logger = {
			error: (details: any, msg: string) => {
				assert.deepStrictEqual(details.err, error, 'should be the details');
				assert.deepStrictEqual(msg, error, 'should be the connection msg');
			}
		};
		const exit = (code: number) => {
			assert.strictEqual(code, 1, 'should be the code to exit');
			done();
		};

		wampClient.openConnection = () => void 0;

		MicroserviceWampModule.init(wampClient, {}, logger, exit);
		wampClient.onError.next(error);
	});

	it('should connect to Crossbar');
});
