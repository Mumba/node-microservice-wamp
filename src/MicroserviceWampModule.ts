/**
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import { DiContainer } from 'dicontainer';
import { Config } from 'mumba-config';
import { WampClient, WampSession, WampChallenge } from 'mumba-wamp';

/**
 * Create the options for the WAMP client, injecting the appropriate challenge function.
 */
function createWampOptions(config: Config): any {
	const wampchallenge = new WampChallenge();
	const options: any = config.get('wamp');

	options.onchallenge = wampchallenge.createWampCra(options.secret);

	return options;
}

export class MicroserviceWampModule {
	/**
	 * Factory to register the WAMP module dependencies in the DI container.
	 */
	public static register(container: DiContainer) {
		container
			.object('exit', (code: number) => process.exit(code))
			.factory('wampOptions', createWampOptions)
			.service('wampClient', ['wampOptions'], WampClient)
			.service('wampSessionApi', WampSession);
	}

	/**
	 * Factory method to initialise the WAMP artifacts.
	 */
	public static init(
		wampClient: WampClient,
		wampOptions: any,
		logger: any,
		exit: any
	) {
		wampClient.onOpen.subscribe((session: any) =>
			logger.debug(
				{
					session_id: session.id
				},
				'WAMP connected'
			)
		);

		wampClient.onClose.subscribe((details: any) =>
			logger.debug(
				{
					details,
					url: wampOptions.url
				},
				'WAMP disconnected'
			)
		);

		wampClient.onError.subscribe((err: any) => {
			logger.error({ err }, err.message ? err.message : err);

			if (err.error && err.error === 'wamp.error.procedure_already_exists') {
				exit(1);
			}
		});

		wampClient.openConnection();
	}
}
