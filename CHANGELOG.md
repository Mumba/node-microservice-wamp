# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased][unreleased]

## [0.3.5] 27 Jul 2018

- Will call an exit function if error handled gets a `wamp.error.procedure_already_exists` error.

## [0.3.4] 3 Apr 2018

- Updated deps.

## [0.3.3] 6 Sep 2017

- Updated deps (fixed cache issue in WampClient).

## [0.3.2] 5 Sep 2017

- Updated deps; replaced typings with @types; added linting.

## [0.3.1] 29 May 2017

- Updated `mumba-wamp` with latest version of Autobahn.

## [0.3.0] 16 Nov 2016

- Updated deps.
- Added error logging in `.init`.

## [0.2.0] 5 Aug 2016

- Changed signature of `configure` to take the required arguments rather than the container.

## [0.1.1] 5 Aug 2016

- Added exports for `mumba-wamp`.

## [0.1.0] 27 Jul 2016

- Initial release
