[![build status](https://gitlab.com/Mumba/node-microservice-wamp/badges/master/build.svg)](https://gitlab.com/Mumba/node-microservice-wamp/commits/master)
# Mumba Microservice WAMP

Provide WAMP support for a Microservice using `mumba-wamp` (via Autobahn).

## DI Container

Adds an instance of `WampClient` object as "wampClient".

Adds an instance of `WampSession` object as "wampSessionApi".

Requires that a `Config` object has been registered as `config`.

When the artifacts resolve, it expects that the WAMP configuration is set in the `wamp` path in the `Config` object.

## Installation

```sh
$ npm install --save mumba-microservice-wamp
$ typings install --save dicontainer=npm:mumba-typedef-dicontainer
```

## Examples

```typescript
const Sandal = require("sandal");
import {DiContainer} from "mumba-typedef-dicontainer";
import {Config} from "mumba-config";
import {MicroserviceWampModule} from "mumba-microservice-wamp";

let container: DiContainer = new Sandal();

// Set the configuration for the logger.
// (or include the Microservice Config Module and set `defaultConfig`)
container.object('config', new Config({
	wamp: {
		url: 'ws://mumba__crossbar:8080',
		realm: 'test-realm',
		authid: '1',
		authmethods: ['wampcra'],
		secret: '*password'
	}
}));

// Register the module.
MicroserviceWampModule.register(container);

container.resolve((err: Error, wampClient: any) => {
	if (err) {
		throw err;
	}

	// Connect to WAMP
});
```

## Tests

To run the test suite, first install the dependencies, then run `npm test`:

```sh
$ npm run docker:up
$ npm install
$ npm test
$ npm docker:down
```

## People

The original author of _Mumba Microservice WAMP_ is [Andrew Eddie](https://gitlab.com/u/aeddie.mumba).

[List of all contributors](https://gitlab.com/Mumba/node-microservice-wamp/graphs/master)

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2016 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.

